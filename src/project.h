#pragma once

#include "action_template.h"

#include <unknown.h>
#include <ptr.h>

#include <string>
#include <vector>

class action;

class project :
	public virtual unknown
{
public:
	virtual std::string const &name() const = 0;

	virtual void add_build_command(unknown *) = 0;

	/// TODO
	virtual std::vector<ptr<action_template>> const &get_build_commands() const = 0;
};

class project_impl :
	public project
{
public:
	project_impl(std::string const &prj_name)
	:
		prj_name(prj_name)
	{ }

	// project
	std::string const &name() const final;
	void add_build_command(unknown *) final;

	std::vector<ptr<action_template>> const &get_build_commands() const final { return build_commands; }

private:
	std::string const prj_name;
	std::vector<ptr<action_template>> build_commands;
};
