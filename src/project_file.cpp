#include "project_file.h"

#include "interp.h"

#include <elim_parse.h>
#include <elim_lex.h>

project_file::project_file(
		environment const &env,
		std::string const &file)
{
	FILE *in = fopen(file.c_str(), "r");
	if(!in)
		throw "Cannot open file";

	statement_vector top;

	yyscan_t scanner;
	yylex_init(&scanner);
	yyset_in(in, scanner);
	int rc = yyparse(scanner, top);
	yylex_destroy(scanner);
	fclose(in);

	interpreter i(env);

	i(top);

	this->projects = i.get_projects();

	if(rc != 0)
		throw "Invalid file";
}
