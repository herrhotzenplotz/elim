#include "print.h"

#include <iostream>

std::string const &print::name() const
{
	return text;
}

void print::do_start()
{
	set_action_state(STARTED);
	std::cerr << text << std::endl;
	set_action_state(FINISHED);
}
