#include "mainloop.h"
#include "output.h"
#include "command.h"

#include "ptr.h"

int main(int, char **)
{
	mainloop m;

	output o;

	auto c = make_object<command, command_impl>(m, "echo test");

	c->set_stdout(o);

	c->start();

	m.run();

	if(o.size() != 1)
		return 1;

	if(o.front() != "test")
		return 1;

	return 0;
}
