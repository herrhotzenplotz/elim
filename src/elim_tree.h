#pragma once

#include <string>
#include <vector>
#include <memory>

struct string_view
{
	char const *text;
	size_t leng;

	operator std::string() const noexcept { return { text, leng }; }
};

struct location
{
	unsigned int first_column, first_line;
	unsigned int last_column, last_line;
};

#define YYLTYPE struct location
#define YYLTYPE_IS_TRIVIAL 1

struct array_expression;
struct function_call_expression;
struct reference_expression;
struct boolean_literal;
struct string_literal;

struct expression_visitor
{
	virtual void visit(array_expression const &) = 0;
	virtual void visit(function_call_expression const &) = 0;
	virtual void visit(reference_expression const &) = 0;
	virtual void visit(boolean_literal const &) = 0;
	virtual void visit(string_literal const &) = 0;
};

struct expression
{
	virtual ~expression() noexcept = default;

	virtual void apply(expression_visitor &) const = 0;
};

typedef std::vector<std::unique_ptr<expression>> expression_vector;

struct expression_list
{
	expression_list() : length(0) { }
	expression_list(expression *const rhs) : rhs(rhs), length(1) { }
	expression_list(expression_list *const lhs, expression *const rhs)
	:
		lhs(lhs), rhs(rhs), length(lhs->length + 1)
	{ }

	std::unique_ptr<expression_list> const lhs;
	// non-const because this needs to be moved out
	std::unique_ptr<expression> rhs;
	unsigned int const length;

	expression_vector convert()
	{
		expression_vector ret;
		expression_vector::size_type i = length;
		ret.resize(i);

		auto ii = this;
		while(i--)
		{
			ret[i] = std::move(ii->rhs);
			ii = ii->lhs.get();
		}

		delete this;
		return ret;
	}
};

struct array_expression : expression
{
	array_expression(
			location loc,
			expression_list *const values)
	:
		loc(loc),
		values(values->convert())
	{ }

	location const loc;

	expression_vector const values;

	void apply(expression_visitor &v) const final { v.visit(*this); }
};

struct argument
{
	argument(
			location loc,
			std::string &&name,
			expression *value)
	:
		loc(loc),
		name(std::move(name)),
		value(value)
	{ }

	location const loc;
	std::string const name;
	std::unique_ptr<expression> const value;
};

typedef std::vector<std::unique_ptr<argument>> argument_vector;

struct argument_list
{
	argument_list() : length(0) { }
	argument_list(argument *rhs) : rhs(rhs), length(1) { }
	argument_list(argument_list *lhs, argument *rhs)
	:
		lhs(lhs), rhs(rhs), length(lhs->length + 1)
	{ }

	std::unique_ptr<argument_list> const lhs;
	// non-const because this needs to be moved out
	std::unique_ptr<argument> rhs;
	unsigned int const length;

	argument_vector convert()
	{
		argument_vector ret;
		argument_vector::size_type i = length;
		ret.resize(i);

		auto ii = this;
		while(i--)
		{
			ret[i] = std::move(ii->rhs);
			ii = ii->lhs.get();
		}

		delete this;
		return ret;
	}
};

struct function_call_expression : expression
{
	function_call_expression(location loc, std::string &&name)
	:
		loc(loc),
		name(std::move(name))
	{ }

	function_call_expression(
			location loc,
			std::string &&name,
			expression_list *const unnamed_args)
	:
		loc(loc),
		name(std::move(name)),
		unnamed_args(unnamed_args->convert())
	{ }

	function_call_expression(
			location loc,
			std::string &&name,
			expression_list *const unnamed_args,
			argument_list *const named_args)
	:
		loc(loc),
		name(std::move(name)),
		unnamed_args(unnamed_args->convert()),
		named_args(named_args->convert())
	{ }

	function_call_expression(
			location loc,
			std::string &&name,
			argument_list *const named_args)
	:
		loc(loc),
		name(std::move(name)),
		named_args(named_args->convert())
	{ }

	location const loc;
	std::string const name;
	expression_vector const unnamed_args;
	argument_vector const named_args;

	void apply(expression_visitor &v) const final { v.visit(*this); }
};

struct reference_expression : expression
{
	reference_expression(location loc, std::string &&name)
	:
		loc(loc), name(std::move(name))
	{ }

	location const loc;
	std::string const name;

	void apply(expression_visitor &v) const final { v.visit(*this); }
};

struct boolean_literal : expression
{
	boolean_literal(bool value) : value(value) { }

	bool const value;

	void apply(expression_visitor &v) const final { v.visit(*this); }
};

struct string_literal : expression
{
	string_literal(location loc, std::string &&value)
	:
		loc(loc), value(value.substr(1, value.size()-2))
	{ }

	location const loc;
	std::string const value;

	void apply(expression_visitor &v) const final { v.visit(*this); }
};

struct expression_statement;
struct assignment_statement;

struct statement_visitor
{
	virtual void visit(expression_statement const &) = 0;
	virtual void visit(assignment_statement const &) = 0;
};

struct statement
{
	virtual ~statement() noexcept = default;

	virtual void apply(statement_visitor &) const = 0;
};

typedef std::vector<std::unique_ptr<statement>> statement_vector;

struct statement_list
{
	statement_list() : length(0) { }
	statement_list(statement *const rhs) : rhs(rhs), length(1) { }
	statement_list(statement_list *const lhs, statement *const rhs)
	:
		lhs(lhs), rhs(rhs), length(lhs->length + 1)
	{ }

	std::unique_ptr<statement_list> const lhs;
	// non-const because this needs to be moved out
	std::unique_ptr<statement> rhs;
	unsigned int const length;

	statement_vector convert()
	{
		statement_vector ret;
		statement_vector::size_type i = length;
		ret.resize(i);

		auto ii = this;
		while(i--)
		{
			ret[i] = std::move(ii->rhs);
			ii = ii->lhs.get();
		}

		delete this;
		return ret;
	}
};

struct expression_statement : statement
{
	expression_statement(location loc, expression *const expr)
	:
		loc(loc), expr(expr)
	{ }

	location const loc;
	std::unique_ptr<expression> const expr;

	void apply(statement_visitor &v) const final { v.visit(*this); }
};

struct assignment_statement : statement
{
	assignment_statement(
			location loc,
			std::string &&name,
			expression *const value)
	:
		loc(loc),
		name(std::move(name)),
		value(value)
	{ }

	location const loc;
	std::string const name;
	std::unique_ptr<expression> const value;

	void apply(statement_visitor &v) const final { v.visit(*this); }
};
