%define api.pure full
%define parse.error verbose
%param {yyscan_t scanner}
%parse-param {statement_vector &top}
%locations

%union {
	string_view identifier;
	string_view string;

	statement *stmt;
	statement_list *stmt_list;

	expression *expr;
	expression_list *expr_list;

	argument *arg;
	argument_list *arg_list;
}

%code requires {
#include "elim_tree.h"

	typedef void *yyscan_t;
}

%code {
#include "elim_lex.h"

/// @todo remove
#include <stdio.h>

void yyerror(YYLTYPE *yylval, yyscan_t, statement_vector &, char const *msg)
{
	fprintf(stderr, "%d:%d: %s\n",
		yylval->first_line, yylval->first_column,
		msg);
}

}

%token end_of_file 0 "end of file"

%token FALSE "false"
%token TRUE "true"

%token<identifier> IDENTIFIER
%token<string> STRING

%type<stmt> statement assignment
%type<stmt_list> statements
%type<expr> expression array function_call reference literal boolean_literal string_literal
%type<expr_list> expression_list
%type<arg> named_argument
%type<arg_list> named_arguments

%destructor { delete $$; } <stmt> <stmt_list> <expr> <expr_list> <arg> <arg_list>

%%

start:
	statements
		{ top = $1->convert(); }

statements:
	statements statement
		{ $$ = new statement_list { $1, $2 }; }
|	%empty
		{ $$ = new statement_list { }; }

statement:
	assignment
		{ $$ = $1; }
|	function_call
		{ $$ = new expression_statement { @1, $1 }; }

assignment:
	IDENTIFIER '=' expression
		{ $$ = new assignment_statement(@1, $1, $3); }

expression:
	array
|	function_call
|	reference
|	literal

array:
	'[' expression_list ']'
		{ $$ = new array_expression(@1, $2); }

expression_list:
	expression_list ',' expression
		{ $$ = new expression_list { $1, $3 }; }
|	expression
		{ $$ = new expression_list { new expression_list, $1 }; }

function_call:
	IDENTIFIER '(' ')'
		{ $$ = new function_call_expression(@1, $1); }
|	IDENTIFIER '(' expression_list ')'
		{ $$ = new function_call_expression(@1, $1, $3); }
|	IDENTIFIER '(' expression_list ',' named_arguments ')'
		{ $$ = new function_call_expression(@1, $1, $3, $5); }
|	IDENTIFIER '(' named_arguments ')'
		{ $$ = new function_call_expression(@1, $1, $3); }

named_arguments:
	named_arguments ',' named_argument
		{ $$ = new argument_list { $1, $3 }; }
|	named_argument
		{ $$ = new argument_list { new argument_list, $1 }; }

named_argument:
	IDENTIFIER ':' expression
		{ $$ = new argument(@3, $1, $3); }

reference:
	IDENTIFIER
		{ $$ = new reference_expression(@1, $1); }

literal:
	boolean_literal
|	string_literal

boolean_literal:
	"true"
		{ $$ = new boolean_literal(true); }
|	"false"
		{ $$ = new boolean_literal(false); }

string_literal:
	STRING
		{ $$ = new string_literal(@1, $1); }
