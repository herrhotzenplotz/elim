#include "interp.h"
#include "interp__exception.h"

#include "command_template.h"
#include "directory.h"

#include <functional>

void interpreter::operator()(statement_vector const &v)
{
	for(auto const &i : v)
		i->apply(*this);
}

void interpreter::visit(expression_statement const &s)
{
	s.expr->apply(*this);
}

void interpreter::visit(assignment_statement const &s)
{
	s.value->apply(*this);
	variables[s.name] = std::move(current);
}

void interpreter::visit(array_expression const &e)
{
	auto new_array = std::make_unique<array_value>();
	new_array->contents.reserve(e.values.size());
	for(auto const &i : e.values)
	{
		i->apply(*this);
		new_array->contents.emplace_back(std::move(current));
	}
	current = std::move(new_array);
}

void interpreter::visit(function_call_expression const &e)
{
	auto ii = functions.find(e.name);
	if(ii == functions.end())
		throw undefined_function(e.name);

	auto const &f = ii->second;

	args_type args;

	function::index_type::size_type idx = 0;
	for(auto const &i : e.unnamed_args)
	{
		i->apply(*this);
		args[f.param_pos[idx++].name] = std::move(current);
	}
	for(auto const &i : e.named_args)
	{
		i->value->apply(*this);
		args[i->name] = std::move(current);
	}

	current = (this->*f.f)(env, args);
}

void interpreter::visit(reference_expression const &e)
{
	auto i = variables.find(e.name);
	if(i == variables.end())
		throw undefined_variable(e.name);
	current.reset(i->second->clone());
}

void interpreter::visit(boolean_literal const &e)
{
	current = std::make_unique<boolean_value>(e.value);
}

void interpreter::visit(string_literal const &e)
{
	current = std::make_unique<string_value>(e.value);
}

std::map<interpreter::symbol, interpreter::function> const interpreter::functions =
{
	{
		"command",
		{
			&interpreter::command_,
			{
				{ "commandline", true }
			}
		}
	},
	{
		"project",
		{
			&interpreter::project_,
			{
				{ "name", true },
				{ "build", true }
			}
		}
	},
	{
		"tempdir",
		{
			&interpreter::tempdir_,
			{
			}
		}
	}
};

std::unique_ptr<value> interpreter::command_(
		environment const &env,
		args_type const &args)
{
	auto const commandline_str = get_required_string("command", "commandline", args);

	return std::make_unique<object_value>(
			make_object<unknown, command_template>(
				env,
				commandline_str));
}

std::unique_ptr<value> interpreter::project_(
		environment const &,
		args_type const &args)
{
	auto const prj_name = get_required_string("project", "name", args);
	auto const build_cmd = get_required_object_array("project", "build", args);

	auto prj = make_object<project, project_impl>(prj_name);
	for(auto &i : build_cmd)
		prj->add_build_command(std::move(i));

	projects[prj_name] = prj;

	// TODO: void type? something else?
	return std::make_unique<boolean_value>(true);
}

std::unique_ptr<value> interpreter::tempdir_(
		environment const &,
		args_type const &)
{
	return std::make_unique<object_value>(
			make_object<directory, directory_impl>(
				directory_impl::temp_directory));
}

value const &interpreter::get_required(
	char const *const func_name,
	char const *const arg_name,
	args_type const &args)
{
	auto const i = args.find(arg_name);
	if(i == args.end())
		throw missing_argument(func_name, arg_name);
	return *i->second;
}

std::string interpreter::get_required_string(
	char const *const func_name,
	char const *const arg_name,
	args_type const &args)
{
	auto const &v = get_required(func_name, arg_name, args);
	if(auto const s = dynamic_cast<string_value const *>(&v))
		return s->contents;
	else
		throw type_mismatch(
			func_name,
			arg_name,
			typeid(v),
			typeid(string_value));
}

std::vector<ptr<unknown>> interpreter::get_required_object_array(
	char const *const func_name,
	char const *const arg_name,
	args_type const &args)
{
	auto const &v = get_required(func_name, arg_name, args);
	std::vector<ptr<unknown>> ret;

	std::function<void(value const &)> append =
	[func_name, arg_name, &append, &ret](value const &v)
	{
		if(auto const a = dynamic_cast<array_value const *>(&v))
			for(auto const &i : a->contents)
				append(*i);
		else if(auto const o = dynamic_cast<object_value const *>(&v))
			ret.emplace_back(o->contents);
		else
			throw type_mismatch(
					func_name,
					arg_name,
					typeid(v),
					typeid(array_value));
	};

	append(v);
	return ret;
}
