#pragma once

#include "elim_tree.h"

#include "project.h"

#include "value.h"

#include <map>

#include <ptr.h>
#include <unknown.h>

class environment;

class interpreter :
	private statement_visitor,
	private expression_visitor
{
public:
	interpreter(environment const &env) : env(env) { }

	void operator()(statement_vector const &);

	class undefined_function;
	class undefined_variable;
	class missing_argument;
	class type_mismatch;

	std::map<std::string, ptr<project>> const &get_projects() const
	{
		return projects;
	}

private:
	environment const &env;

	void visit(expression_statement const &) final;
	void visit(assignment_statement const &) final;

	void visit(array_expression const &) final;
	void visit(function_call_expression const &) final;
	void visit(reference_expression const &) final;
	void visit(boolean_literal const &) final;
	void visit(string_literal const &) final;

	typedef std::string symbol;

	std::unique_ptr<value> current;

	std::map<symbol, std::unique_ptr<value>> variables;

	std::map<std::string, ptr<project>> projects;

	struct param_info
	{
		symbol name;
		bool optional;
	};

	typedef std::map<symbol, std::unique_ptr<value>> args_type;

	typedef std::unique_ptr<value> (interpreter::*builtin)(environment const &, args_type const &);

	struct function
	{
		typedef std::map<symbol, std::vector<param_info>::size_type> index_type;

		function(
				builtin f,
				std::initializer_list<param_info> p)
	:
		f(f),
		param_pos(p),
		param_names(index(p))
	{ }

		builtin const f;
		std::vector<param_info> const param_pos;
		index_type const param_names;

	private:
		index_type index(std::initializer_list<param_info> p)
		{
			index_type ret;
			for(auto ii = p.begin(); ii != p.end(); ++ii)
				ret[ii->name] = ii - p.begin();
			return ret;
		}
	};

	static std::map<symbol, function> const functions;

	std::unique_ptr<value> command_(environment const &, args_type const &);
	std::unique_ptr<value> project_(environment const &, args_type const &);
	std::unique_ptr<value> tempdir_(environment const &, args_type const &);

	value const &get_required(
		char const *func_name,
		char const *arg_name,
		args_type const &args);
	std::string get_required_string(
		char const *func_name,
		char const *arg_name,
		args_type const &args);
	std::vector<ptr<unknown>> get_required_object_array(
		char const *func_name,
		char const *arg_name,
		args_type const &args);
};
