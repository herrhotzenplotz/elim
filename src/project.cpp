#include "project.h"

std::string const &project_impl::name() const
{
	return prj_name;
}

void project_impl::add_build_command(unknown *unkcmd)
{
	action_template *a = dynamic_cast<action_template *>(unkcmd);

	if(!a)
		throw 0;

	build_commands.push_back(a);
}
