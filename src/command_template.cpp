#include "command_template.h"

#include "command.h"

#include "environment.h"

void command_template::create_action(action **out_action) const
{
	(*out_action = make_object<action, command_impl>(env.m, cmd))->add_ref();
}
