#pragma once

#include "action.h"

class print :
	public action_impl
{
public:
	template<typename ...Deps>
	print(std::string const &text, Deps &&...dependencies)
	:
		action(std::forward<Deps>(dependencies)...),
		text(text)
	{
	}

	std::string const &name() const override;

private:
	void do_start();

	std::string const text;
};
