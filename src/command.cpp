#include "command.h"

#include "mainloop.h"

#include <memory>

#include <sys/types.h>
#include <sys/wait.h>

#include <pwd.h>

#include <cstring>

#include <unistd.h>

void command_impl::set_stdout(output &o)
{
	this->o = &o;
}

void command_impl::do_start()
{
	set_action_state(STARTED);

	char const *const path = "/bin/sh";

	char sh[] = "sh";
	char dash_c[] = "-c";

	auto shell_command = std::make_unique<char []>(m_shell_command.size() + 1);
	m_shell_command.copy(shell_command.get(), m_shell_command.size());
	shell_command[m_shell_command.size()] = '\0';

	char *const argv[] =
	{
		sh,
		dash_c,
		shell_command.get(),
		nullptr
	};

	std::vector<char> env;
	std::vector<size_t> envo;

	auto add_env = [&env, &envo](char const *name, char const *value)
	{
		auto const name_len = strlen(name);
		auto const value_len = strlen(value);
		auto const old_size = env.size();
		envo.emplace_back(old_size);
		env.resize(old_size + name_len + value_len + 2);
		char *o = env.data() + old_size;
		memcpy(o, name, name_len);
		o += name_len;
		*o++ = '=';
		memcpy(o, value, value_len);
		o += value_len;
		*o++ = '\0';
	};

	auto p = getpwuid(getuid());

	add_env("PATH", "/usr/bin:/bin");
	add_env("USER", p->pw_name);
	add_env("HOME", p->pw_dir);
	add_env("SHELL", p->pw_shell);

	std::vector<char *> envp(envo.size() + 1);
	{
		char **o = envp.data();
		for(auto const &i : envo)
			*o++ = env.data() + i;
	}

	int stdout_pipe[2];

	if(o)
	{
		int rc = ::pipe(stdout_pipe);
		if(rc != 0)
			throw errno;
	}

	pid_t pid = fork();
	switch(pid)
	{
	case -1:
		throw errno;

	case 0:
		if(o)
		{
			::close(stdout_pipe[0]);
			::dup2(stdout_pipe[1], STDOUT_FILENO);
			::close(stdout_pipe[1]);
		}

		::_exit(::execve(path, argv, envp.data()));

	default:
		if(o)
		{
			::close(stdout_pipe[1]);
			m.connect(stdout_pipe[0], *o);
		}

		this->pid = pid;
		m.watch_process(this);
		return;
	}
}

void command_impl::on_process_exit(int)
{
	set_action_state(FINISHED);
}
