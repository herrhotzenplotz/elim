%option 8bit
%option never-interactive
%option noyywrap
%option nounput
%option nodefault
%option bison-bridge
%option bison-locations
%option reentrant
%option warn
%option yylineno

%{
#include "elim_parse.h"

/// @todo remove
#include <stdio.h>

#define YY_USER_ACTION \
	{ \
		yycolumn += yyleng; \
		yylloc->first_line = yylloc->last_line; \
		yylloc->first_column = yylloc->last_column; \
		yylloc->last_line = yylineno; \
		yylloc->last_column = yycolumn + 1; \
	}

%}

IDENTIFIER	[A-Za-z_][A-Za-z0-9_]*
STRING		'[^']*'

%%

\t*		/* ignore */
\ *		/* ignore */
\n*		yycolumn = 0; /* ignore */

\(		return '(';
\)		return ')';
,		return ',';
:		return ':';
=		return '=';
\[		return '[';
\]		return ']';

false		return FALSE;
true		return TRUE;

{IDENTIFIER}	{
			yylval->identifier.text = yytext;
			yylval->identifier.leng = yyleng;
			return IDENTIFIER;
		}

{STRING}	{
			yylval->string.text = yytext;
			yylval->string.leng = yyleng;
			return STRING;
		}

.		{
			fprintf(stderr, "%d:%d: Invalid character %02x\n",
			yylineno, yycolumn, yytext[0]);
			return YYerror;
		}
