#include "directory.h"

#include <memory>

#include <cstdlib>
#include <sys/stat.h>

directory_impl::directory_impl(temp_directory_tag const &)
:
	path(create_temp_directory()),
	delete_on_destroy(true)
{
}

directory_impl::~directory_impl() noexcept
try
{
	if(delete_on_destroy)
		remove_all(path);
}
catch(const std::filesystem::filesystem_error &)
{
}

std::filesystem::path directory_impl::create_temp_directory()
{
	for(;;)
	{
		std::unique_ptr<char, decltype(::free) *> const name(
			tempnam(nullptr, nullptr),
			::free);
		if(!name)
			throw 0;
		int rc = ::mkdir(name.get(), 0700);
		if(rc == 0)
		{
			return { name.get() };
		}
		if(errno == EEXIST)
			continue;
		throw 0;
	}
}
