#pragma once

#include <list>

#include <unistd.h>

class output;

class mainloop
{
public:
	void run();

	class process_watcher
	{
	public:
		pid_t pid;
		virtual void on_process_exit(int status) = 0;
	};

	void watch_process(process_watcher *);

	void connect(int fd, output &o);

private:
	std::list<process_watcher *> m_processes;

	struct output_pipe
	{
		int fd;
		output &o;
	};

	std::list<output_pipe> m_outputs;
};
