#pragma once

#include "unknown.h"

#include <utility>

#include <cassert>

template<typename T>
class ptr
{
public:
	ptr() : p(nullptr) { }

	ptr(std::nullptr_t) : p(nullptr) { }
	
	template<typename U>
	ptr(U *u) : p(u)
	{
		if(p)
			p->add_ref();
	}

	ptr(ptr const &u) : p(u.get())
	{
		if(p)
			p->add_ref();
	}

	template<typename U>
	ptr(ptr<U> const &u) : p(u.get())
	{
		if(p)
			p->add_ref();
	}

	ptr(ptr &&u) : p(u.detach()) { }

	template<typename U>
	ptr(ptr<U> &&u) : p(u.detach()) { }

	~ptr() noexcept
	{
		if(p)
			p->release();
	}

	ptr &operator=(std::nullptr_t)
	{
		if(p)
			p->release();
		p = nullptr;
		return *this;
	}

	template<typename U>
	ptr &operator=(U *u)
	{
		auto old_unk = static_cast<unknown *>(p);
		auto new_unk = static_cast<unknown *>(u);

		if(old_unk != new_unk)
		{
			if(new_unk)
				new_unk->add_ref();
			if(old_unk)
				old_unk->release();
		}

		p = u;
		return *this;
	}

	ptr &operator=(ptr const &rhs)
	{
		if(this == &rhs)
			return *this;

		auto old_unk = static_cast<unknown *>(p);
		auto new_unk = static_cast<unknown *>(rhs.get());

		if(old_unk != new_unk)
		{
			if(new_unk)
				new_unk->add_ref();
			if(old_unk)
				old_unk->release();
		}

		p = rhs.get();
		return *this;
	}
		
	template<typename U>
	ptr &operator=(ptr<U> const &rhs)
	{
		auto old_unk = static_cast<unknown *>(p);
		auto new_unk = static_cast<unknown *>(rhs.get());

		if(old_unk != new_unk)
		{
			if(new_unk)
				new_unk->add_ref();
			if(old_unk)
				old_unk->release();
		}

		p = rhs.get();
		return *this;
	}

	ptr &operator=(ptr &&rhs)
	{
		/* self move is forbidden */
		assert(this != rhs.addr());

		if(this == rhs.addr())
			return *this;

		reset();
		attach(rhs.detach());
		return *this;
	}

	template<typename U>
	ptr &operator=(ptr<U> &&rhs)
	{
		reset();
		attach(rhs.detach());
		return *this;
	}

	operator T*() const noexcept { return p; }
	T *operator->() const noexcept { return p; }
	T &operator*() const noexcept { return *p; }
	T *get() const noexcept { return p; }

	T **operator&()
	{
		assert(!p);
		return &p;
	}

	void reset() noexcept
	{
		if(p)
			p->release();
		p = nullptr;
	}

	void attach(T *obj) noexcept
	{
		p = obj;
	}

	T *detach() noexcept
	{
		T *ret = p;
		p = nullptr;
		return ret;
	}
	
private:
	T *p;

	ptr const *addr() const { return this; }
};

template<typename T>
bool operator==(ptr<T> const &lhs, std::nullptr_t)
{
	return lhs.get() == nullptr;
}

template<typename T>
bool operator==(std::nullptr_t, ptr<T> const &rhs)
{
	return nullptr == rhs.get();
}

template<typename I, typename T, typename ...Args>
ptr<I> make_object(Args &&...args)
{
	return ptr<I>(new unknownimpl<T>(std::forward<Args>(args)...));
}
