#pragma once

#include <utility>

#include <cstdint>

/** semi-traditional IUnknown */
class unknown
{
public:
	virtual uint32_t add_ref() = 0;
	virtual uint32_t release() = 0;

protected:
	~unknown() noexcept = default;

	virtual void final_construct() { }
};

template<typename T>
class unknownimpl final :
	public virtual unknown,
	public T
{
public:
	template<typename ...Args>
	unknownimpl(Args &&...args)
	:
		T(std::forward<Args>(args)...)
	{
		this->final_construct();
	}

	uint32_t add_ref() final
	{
		return ++refcount;
	}

	uint32_t release() final
	{
		if(--refcount)
			return refcount;
		delete this;
		return 0;
	}

private:
	/** @todo does not need to be virtual */
	~unknownimpl() noexcept = default;

	uint32_t refcount = 0;
};
