FROM debian:bookworm
RUN apt-get update
RUN apt-get --yes --no-install-recommends install \
	bison \
	ca-certificates \
	flex \
	g++ \
	gcovr \
	meson \
	valgrind
